exports = module.exports = ($sequelize, $database) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.INTEGER, primaryKey:true, autoIncrement: true },
        name: { type: $sequelize.STRING, allowNull: true },
        lastName: { type: $sequelize.STRING, allowNull: true },
        momLastName: { type: $sequelize.STRING, allowNull: true },
        rfc: { type: $sequelize.STRING, allowNull: true, unique: true }
    };

    const hooks = {
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('client', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

// Dependencies
exports['@require'] = ['sequelize', 'common/database'];