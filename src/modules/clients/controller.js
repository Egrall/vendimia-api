exports = module.exports = ($repository, $result) => {
    return {
        getAll: (req, res) => {
            $repository.getAll()
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        },
        search: (req, res) => {
            $repository.search(req.params.key)
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        }
    };
};

exports['@singleton'] = true;
exports['@require'] = ['clients/repository', 'api/result'];