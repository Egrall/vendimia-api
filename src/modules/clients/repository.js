exports = module.exports = ($model, $database, $sequelize) => {
    const Op = $sequelize.Op;
    let connection = $database.connect();
    return {
        getAll: ()=>{
            return new Promise((resolve, reject)=>{
                $model.findAll({where: {name:{[Op.ne]: null}}})
                .then(data => {
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                });
            });
        },
        search: (key) => {
            return new Promise((resolve, reject) => {
                let criteria = {
                    where: $sequelize.where($sequelize.fn("concat", $sequelize.col("name"), ' ', $sequelize.col("lastName"),' ', $sequelize.col("momLastName")), {
                    [Op.like]: '%'+key+'%'
                })};
                $model.findAll(criteria).then(data => {
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                })
            });
        }
    };
};

exports['@require'] = ['clients/model', 'common/database', 'sequelize'];