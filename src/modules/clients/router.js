exports = module.exports = ($express, $controller) => {
    let router = $express.Router();

    router.get('/list', $controller.getAll);
    router.get('/search/:key', $controller.search);
    return router;
};


exports['@singleton'] = true;
exports['@require'] = ['express','clients/controller'];