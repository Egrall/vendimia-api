exports = module.exports = ($sequelize, $database) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.INTEGER, primaryKey:true, autoIncrement: true },
        sale: { type: $sequelize.INTEGER, allowNull: false },
        article: { type: $sequelize.INTEGER, allowNull: false },
        articleDescription: { type: $sequelize.STRING, allowNull: false},
        articleModel: { type: $sequelize.STRING, allowNull: false},
        quantity: { type: $sequelize.INTEGER, allowNull: false },
        price: { type: $sequelize.DOUBLE, allowNull: false },
        amount: { type: $sequelize.DOUBLE, allowNull: false }
    };

    const hooks = {
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('detailSale', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

// Dependencies
exports['@require'] = ['sequelize', 'common/database'];