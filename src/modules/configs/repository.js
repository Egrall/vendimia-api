exports = module.exports = ($model, $database, $sequelize) => {
    const Op = $sequelize.Op;
    let connection = $database.connect();
    return {
        getAll: () => $model.all(),
        findOrCreate: (data) => {
            return new Promise((resolve, reject) =>{
                $model.findOrCreate({where: {key: data.key}, defaults: {value: data.value}})
                .spread((instance, created) => {
                    if(!created){
                        instance.update(data)
                            .then(self => {
                                let updated = self.get({plain: true, attributes: ['id']});
                                resolve({id: updated.id});
                            })
                            .catch(error => {
                                reject(error);
                            });
                    }
                    resolve({id: instance.id})
                })
            });
        }
    };
};

exports['@require'] = ['configs/model', 'common/database', 'sequelize'];