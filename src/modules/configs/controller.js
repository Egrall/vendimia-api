exports = module.exports = ($repository, $result) => {
    return {
        getAll: (req, res) => {
            $repository.getAll()
            .then(result => {
                let response = {};
                result.map((item, i, array)=>{
                    response[item.key] = item.value
                });
                res.json($result.success(response));
            }).catch(error => {
                res.json($result.error(error));
            });
        },
        findOrCreate: (req, res) => {
            $repository.findOrCreate(req.body)
            .then(result => {
                res.json($result.success(result));
            })
            .catch(error => {
                res.json($result.error(error));
            });
        }
    };
};

exports['@singleton'] = true;
exports['@require'] = ['configs/repository', 'api/result'];