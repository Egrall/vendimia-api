exports = module.exports = ($express, $controller) => {
    let router = $express.Router();
    router.post('/updateConfig', $controller.findOrCreate);
    return router;
};

exports['@singleton'] = true;
exports['@require'] = ['express','configs/controller'];