exports = module.exports = ($sequelize, $database) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.INTEGER, primaryKey:true, autoIncrement: true },
        key: { type: $sequelize.STRING, allowNull: false },
        value: { type: $sequelize.DOUBLE, allowNull: false },
    };

    const hooks = {
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('config', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

// Dependencies
exports['@require'] = ['sequelize', 'common/database'];