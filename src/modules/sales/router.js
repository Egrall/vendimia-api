exports = module.exports = ($express, $controller) => {
    let router = $express.Router();

    router.get('/list', $controller.getAll);
    return router
};


exports['@singleton'] = true;
exports['@require'] = ['express','sales/controller'];