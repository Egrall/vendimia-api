exports = module.exports = ($repository, $result) => {
    return {
        getAll: (req, res) => {
            $repository.getAll()
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        }
    };
};

exports['@singleton'] = true;
exports['@require'] = ['sales/repository', 'api/result'];