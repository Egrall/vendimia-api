exports = module.exports = ($sequelize, $database) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.INTEGER, primaryKey:true, autoIncrement: true },
        client: { type: $sequelize.INTEGER, allowNull: true },
        clientName: { type: $sequelize.STRING, allowNull: true },
        deposit: { type: $sequelize.DOUBLE, allowNull: true},
        depositBon: { type: $sequelize.DOUBLE, allowNull: true},
        total: { type: $sequelize.DOUBLE, allowNull: true },
        status: { type: $sequelize.STRING, allowNull: true },
        months: { type: $sequelize.INTEGER, allowNull: true },
        amount: { type: $sequelize.DOUBLE, allowNull: true},
        financingRate: { type: $sequelize.DOUBLE, allowNull: false},
        depositP: { type: $sequelize.DOUBLE, allowNull: false},
        deadline: { type: $sequelize.DOUBLE, allowNull: false}
    };

    const hooks = {
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('sale', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

// Dependencies
exports['@require'] = ['sequelize', 'common/database'];