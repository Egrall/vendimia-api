exports = module.exports = ($model, $database, $sequelize) => {
    const Op = $sequelize.Op;
    let connection = $database.connect();
    return {
        getAll: ()=>{
            return new Promise((resolve, reject)=>{
                $model.findAll({where: {client:{[Op.ne]: null}}})
                .then(data => {
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                });
            });
        }
    };
};

exports['@require'] = ['sales/model', 'common/database', 'sequelize'];