exports = module.exports = ($sequelize, $database) => {
    let $connection = $database.connect();

    const schema = {
        id: { type: $sequelize.INTEGER, primaryKey:true, autoIncrement: true },
        description: { type: $sequelize.STRING, allowNull: true },
        model: { type: $sequelize.STRING, allowNull: true },
        price: { type: $sequelize.DOUBLE, allowNull: true },
        stock: { type: $sequelize.INTEGER, allowNull: true}
    };

    const hooks = {
        beforeUpdate: function (item, options) {
            return new Promise ((resolve, reject) => {
                if(item.id)
                    delete item.id;
                resolve(item);
            });
        }
    };

    const Model = $connection.define('article', schema ,{
        hooks: hooks,
        defaultScope: {attributes: { exclude: ['updatedAt','createdAt','deletedAt'] }},
        paranoid: true
    });
    
    return Model;
};

// Dependencies
exports['@require'] = ['sequelize', 'common/database'];