exports = module.exports = ($repository, $result) => {
    return {
        getAll: (req, res) => {
            $repository.getAll()
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        },
        search: (req, res) => {
            $repository.search(req.params.key)
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        },
        addStock: (req, res) => {
            $repository.addStock(req.params.id, req.body)
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        },
        substractStock: (req, res) => {
            $repository.substractStock(req.params.id, req.body)
            .then(result => {
                res.json($result.success(result));
            }).catch(error => {
                res.json($result.error(error));
            });
        }
    };
};

exports['@singleton'] = true;
exports['@require'] = ['articles/repository', 'api/result'];