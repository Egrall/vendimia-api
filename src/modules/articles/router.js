exports = module.exports = ($express, $controller) => {
    let router = $express.Router();

    router.get('/list', $controller.getAll);
    router.get('/search/:key', $controller.search);
    router.post('/addStock/:id', $controller.addStock);
    router.post('/substractStock/:id', $controller.substractStock);
    return router
};


exports['@singleton'] = true;
exports['@require'] = ['express','articles/controller'];