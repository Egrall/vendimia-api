exports = module.exports = ($model, $database, $sequelize) => {
    const Op = $sequelize.Op;
    let connection = $database.connect();
    return {
        getAll: ()=>{
            return new Promise((resolve, reject)=>{
                $model.findAll({where: {description:{[Op.ne]: null}}})
                .then(data => {
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                });
            });
        },
        search: (key) => {
            return new Promise((resolve, reject) => {
                let criteria = {
                    where: { description: { [Op.like]: '%'+key+'%' }}
                };
                $model.findAll(criteria).then(data => {
                    resolve(data);
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        addStock: (id, data) => {
            return new Promise((resolve, reject) => {
                $model.findById(id).then(instance => {
                    const val = parseInt(instance.stock) + parseInt(data.stock);
                    instance.update({stock: val})
                            .then(self => {
                                let updated = self.get({plain: true, attributes: ['id']});
                                resolve({id: updated.id});
                            })
                            .catch(error => {
                                reject(error);
                            });
                })
                .catch(error => {
                    reject(error);
                })
            });
        },
        substractStock: (id, data) => {
            return new Promise((resolve, reject) => {
                $model.findById(id).then(instance => {
                    const val = parseInt(instance.stock) - parseInt(data.stock);
                    if(val < 0 || instance.stock === 0) {
                        reject({message: 'negative'});
                    } else {
                        instance.update({stock: val})
                        .then(self => {
                            let updated = self.get({plain: true, attributes: ['id']});
                            resolve({id: updated.id});
                        })
                        .catch(error => {
                            reject(error);
                        });
                    }
                })
                .catch(error => {
                    reject(error);
                })
            });
        }

    };
};

exports['@require'] = ['articles/model', 'common/database', 'sequelize'];