exports = module.exports = ($database) => {
    let connection = $database.connect();
    return model => {
        return {
            create: data => {
                return new Promise((resolve, reject) => {
                    connection.sync().then(() => {
                        return model.create(data);
                    })
                    .then(data => {
                        let saved = data.get({plain: true, attributes: ['id']});
                        resolve({id: saved.id});
                    })
                    .catch(error => {
                        reject(error);
                    });
                });
            },
            findAll: () => model.all(),
            findById: id => model.findById(id),
            update: (id,data) =>{
                return new Promise((resolve, reject) => {
                    connection.sync().then(() => {
                        model.findById(id)
                        .then(result => {
                            result.update(data)
                            .then(self => {
                                let updated = self.get({plain: true, attributes: ['id']});
                                resolve({id: updated.id});
                            })
                            .catch(error => {
                                reject(error);
                            });
                        })
                        .catch(error => {
                            reject(error);
                        });
                    })
                    .catch(error => {
                        reject(error);
                    });
                });
            },
            delete: (id,data) =>{
                return new Promise((resolve, reject) => {
                    connection.sync().then(() => {
                        return model.findById(id)
                        .then(result => {
                            return result.destroy()
                            .then(self => {
                                let updated = self.get({plain: true, attributes: ['id','deletedAt']});
                                resolve({id: updated.id, deletedAt: updated.deletedAt});
                            });
                        });
                    })
                    .catch(error => {
                        reject(error);
                    });
                });
            }
        };
    };
};
// Dependencies
exports['@require'] = ['common/database'];