module.exports = exports = ($settings, $result, $jwt, $helper) => {
    let config = $settings.get();
    return {
        middleware: middleware,
        sign: sign,
        verify: verify
    };
    /////////////////////////
    
    function middleware (req, res, next) {
        let token = $helper.getToken(req);
        if (token) {
            verify(token)
                .then(decoded => {
                    req.decoded = decoded;
                    next();
                })
                .catch(error => {
                    res.json($result.session());
                });
        } else {
            return res.json($result.session());
        }
    }

    function sign (data) {
        return new Promise((resolve, reject) => {
            try {
                resolve($jwt.sign(data, config.encrypt.secret, {expiresIn: "1d"}));
            } catch (except) {
                reject(except);
            }
        });
    }

    function verify (token) {
        return new Promise ((resolve, reject) => {
            $jwt.verify(token, config.encrypt.secret, (error, decoded) => {
                if (error) { 
                    return reject(error); 
                } else {
                    return resolve(decoded);
                }
            });
        });
    }

};

exports['@singleton'] = true;
exports['@require'] = ['base/settings', 'api/result', 'jsonwebtoken', 'common/helper'];