exports = module.exports = ($express, $cors, $morgan, $bodyParser) => {

    let app = $express();
    let port = process.env.PORT || 3000;

    // middlewares
    app.use($cors());
    app.use(function(req, res, next){
        if(req.url=="/favicon.ico"){
            next();
        }else{
            $morgan('dev')(req, res, next);
        }
    });
    app.use($bodyParser.urlencoded({extended: false}));
    app.use($bodyParser.json());

    app.run = run;

    return app;

    function run(){
        this.listen(port, () => {
            console.log("Server running at port:",port);
        });
    }
};

exports['@singleton'] = true;
exports['@require'] = ['express', 'cors', 'morgan', 'body-parser']