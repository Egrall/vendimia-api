const IoC = require('electrolyte');

const __cwd = process.cwd();
const config = require(`${__cwd}/root.config.json`);
// Load base libs
IoC.use(IoC.node_modules());
IoC.use('base', IoC.dir(`${__dirname}/base`));
IoC.use('api', IoC.dir(`${__dirname}/api`));
IoC.use('common', IoC.dir(`${__dirname}/common`));

IoC.create('base/settings')
.then(settings =>{
    settings.set(config);
    IoC.create('common/database')
    .then(database => {
        database.connect();
        Promise.all([IoC.create('api/app'), IoC.create('common/loader'),
        IoC.create('api/router')])
        .then(DI => {
            let api = DI[0];
            let loader = DI[1]
            let router = DI[2];
            loader.load(api);
            api.use('/', router);
            api.run();
        })
        .catch(error => {
            console.log(error);
        });
    })
    .catch(error => {
        console.log(error);
    });
})
.catch(error => {
    console.log(error);
});