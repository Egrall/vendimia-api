exports = module.exports = ($fs, $settings) => {
    const config = $settings.get();
    return {
        subDirectories: subDirectories,
        getToken: getToken
    };

    ///////////////////////////
    function subDirectories (path) {
        return $fs.readdirSync(path)
            .filter((file) => $libs.fs.statSync(`${path}/${file}`)
                .isDirectory());
    }

    function getToken (request) {
        return request.body.token || request.query.token || 
            request.headers[config.headers.session] || 
            request.cookies[config.cookies.session];
    }
};

// Decorators
exports['@singleton'] = true;
exports['@require'] = ['fs', 'base/settings'];