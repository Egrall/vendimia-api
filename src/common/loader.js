exports = module.exports = ($fs, $settings) => {
    const config = $settings.get();
    const __cwd = process.cwd();
    let IoC = require('electrolyte');

    let modules = $fs.readdirSync(`${__cwd}/${config.modules}`);

    return {
        load: app =>{
            modules.map(name => {
                IoC.use(name, IoC.dir(`${__cwd}/${config.modules}/${name}`));
                IoC.create(`${name}/model`)
                .then(model => {
                    model.sync().then(result => {
                        console.log('Model', `${model.name} synced`);
                        $fs.exists(`${__cwd}/${config.modules}/${name}/router.js`, function(exists){
                            if(exists){
                                IoC.create(`${name}/router`)
                                .then(router => {
                                    let route = `/endpoint/${name}`;
                                    app.use(route, router);
                                    console.log('Router', `${name} [${route}]`);
                                    return `${__cwd}/${config.modules}/${name}`;
                                })
                                .catch(error => {
                                    console.log(error);
                                });
                            }
                        });
                    });
                })
                .catch(error => {
                    console.log(error);
                });
            });
        }
    };
};

exports['@singleton'] = true;
exports['@require'] = ['fs', 'base/settings'];