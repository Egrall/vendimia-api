exports = module.exports = ($sequelize, $settings) => {
    let connection = null;
    return {
        connect: () => {
            const config = $settings.get();
            const db = config.database;
            if (!connection) {
                connection = new $sequelize(db.name, db.username, db.password, {
                    host: db.host,
                    dialect: db.dialect,
                    pool: { max: 5, min: 0, idle: 10000 },
                    logging: false,
                    operatorsAliases: false
                });  
                connection.sync().then(() => 
                    console.log('Common', 'Database', `${db.dialect} [${db.host}/${db.name}]`)
                );
            }
            return connection;
        },
        connection: connection
    };
};

exports['@singleton'] = true;
exports['@require'] = ['sequelize', 'base/settings'];