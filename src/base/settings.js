exports = module.exports = () => {
    let config = {};
    return {
        get: get,
        set: set,
        get apiHost () { 
            return host(config.api);
        },
        get webHost () {
            return host(config.web);
        }
    }
    ///////////////////////
    function get () {
        return config;
    }

    function set (_config = {}) {
        config = require(`${__dirname}/config.json`);
        config = Object.assign({}, config, _config);
    }

    function host (config) {
        return `${(config.ssl?'https':'http')}://${config.hostname}:${config.port}`;
    }
};

exports['@singleton'] = true;